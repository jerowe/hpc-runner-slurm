package HPC::Runner::Node;

use Moose;
use IO::File;
use IO::Select;
use IPC::Open3;
use Symbol;
use IPC::Cmd qw[can_run run];

extends 'HPC::Runner::Slurm';

use Data::Dumper;

=head2 infile
turn this off, just testing the nodes
=cut

has '+infile' => (
    required => 0,
);

has 'all' => (
    is => 'rw',
    traits  => ['Bool'],
    default => 0,
    isa     => 'Bool',
);

=head2 go
Start doing the node check
=cut
sub  go {
    my $self = shift;
    
    $self->get_nodes;

#    my $node = $self->nodelist->[0];
    foreach my $node (@{$self->nodelist}) {

        my $cmd = "srun --partition ".$self->partition." --nodelist ".$node." /home/guests/jir2004/perlmodule/HPC-Runner-Slurm/script/cpus.pl ";

        print "\n========================================================\n";
        print "$cmd\n";

        print "Report Node: ".$node."Partition: ".$self->partition."\n";
        print "========================================================\n\n";


        ### in list context ###
        my( $success, $error_code, $full_buf, $stdout_buf, $stderr_buf ) =
        run( command => $cmd, verbose => 0, timeout => 100 );

        if( $success ) {
            print "this is what the command printed:\n";
            print join "", @$full_buf;
        }
        else{
            print "Command was not successful.\n";
            print "Error Code: $error_code\n";
            print join(" ", @$full_buf)."\n";
        }

    }
}


1;
